<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    $file = file(storage_path('games/20171018-140044.log'));

    $lines = collect($file)
        ->map(function ($line) {
            // map the lines into something usable
            return parseData($line);
        })
        // map over them again to remove anything chat like
        ->filter(function ($line) {
            if (substr(array_get($line, 'text'), 0, 1) === '.') {
                return true;
            }
            if (array_get($line, 'nick') === 'monop') {
                return true;
            }

            return false;
        })
        ->tap(function ($line) {
            return dump(json_encode($line, JSON_PRETTY_PRINT));
        })
        // try and make a command => response combo

        // see what happens then?!
    ;

    // dd(json_encode($lines, JSON_PRETTY_PRINT));
});

function parseData($line)
{
    $line = str_replace(array("\r", "\n", "\r\n", "\n\r"), '', $line);

    preg_match('/^\[([a-zA-Z0-9\ \:\-]+ [a-zA-Z0-9\ \:\-]+)\]/s', $line, $time);
    preg_match('/\<(.*?)\>/s', $line, $nick);
    preg_match('/\> (.*?)$/s', $line, $text);

    // dump($line);
    // dump($time);
    // dump($nick);
    // dump($text);
    // exit;
    return [
        'time' => \Carbon\Carbon::parse(array_get($time, 1))->format('d-m-Y h:i:s # U'),
        'nick' => array_get($nick, 1),
        'text' => array_get($text, 1),
    ];
}
