<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SplitLog extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'split-log';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $file = file_get_contents(storage_path('games/#monop.txt'));

        $regex = '/^\[([a-zA-Z0-9\ \:\[\]\-]+ [a-zA-Z0-9\ \:\[\]\-]+\]\s\<monop\> How many players\?)\s(.*?)\s\[([a-zA-Z0-9\ \:\[\]\-]+ [a-zA-Z0-9\ \:\[\]\-]+\] \<monop\> That\'s a grand worth of [\$0-9]+)\./sm';
        preg_match_all($regex, $file, $matches);

        $this->line('Found ' . count($matches[0]) . ' games...exporting them to files');
        foreach ($matches[0] as $idx => $match) {
            preg_match('/[a-zA-Z0-9\ \:\-]+ [a-zA-Z0-9\ \:\-]+/sm', $matches[1][$idx], $startTime);

            $filename = str_replace(
                [':', '-', ' '],
                ['', '', '-'],
                $startTime[0]
            ) . '.log';

            $filepath = storage_path('games/' . $filename);
            $this->info('Writing ' . $filepath);
            file_put_contents($filepath, $match);
        }
    }
}
